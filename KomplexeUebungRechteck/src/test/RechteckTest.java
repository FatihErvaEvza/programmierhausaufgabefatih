package test;

import model.Rechteck;
import controller.BunteRechteckeController;
public class RechteckTest {
	public static void main(String[] args) {

		Rechteck rechteck0 = new Rechteck();
		rechteck0.setY(10);
		rechteck0.setX(10);
		rechteck0.setHoehe(30);
		rechteck0.setBreite(40);

		Rechteck rechteck1 = new Rechteck();
		rechteck1.setY(25);
		rechteck1.setX(25);
		rechteck1.setHoehe(100);
		rechteck1.setBreite(20);

		Rechteck rechteck2 = new Rechteck(); // Parameterlose Rechtecke von Rechteck 0 bis Rechteck 4
		rechteck2.setY(260);
		rechteck2.setX(10);
		rechteck2.setHoehe(200);
		rechteck2.setBreite(100);

		Rechteck rechteck3 = new Rechteck();
		rechteck3.setY(5);
		rechteck3.setX(500);
		rechteck3.setHoehe(300);
		rechteck3.setBreite(25);
 
		Rechteck rechteck4 = new Rechteck();
		rechteck4.setY(100);
		rechteck4.setX(100);
		rechteck4.setHoehe(100);
		rechteck4.setBreite(100);

		Rechteck rechteck5 = new Rechteck(200, 200, 200, 200);
		Rechteck rechteck6 = new Rechteck(800, 400, 20, 20);
		Rechteck rechteck7 = new Rechteck(800, 450, 20, 20);
		Rechteck rechteck8 = new Rechteck(850, 400, 20, 20);
		Rechteck rechteck9 = new Rechteck(855, 455, 25, 25);

		System.out.println(rechteck0.toString().equals("Rechteck [x=10, y=10, breite=40, hoehe=30]")); // Die Werte waren bei mir anders, in der Console kam dadurch FALSE, weswegen ich die breite und hoehe vertauscht habe.
		System.out.println(rechteck0.toString());

		BunteRechteckeController controller = new BunteRechteckeController();
		controller.add(rechteck0);
		controller.add(rechteck1);
		controller.add(rechteck2);
		controller.add(rechteck3);
		controller.add(rechteck4);
		controller.add(rechteck5);
		controller.add(rechteck6);
		controller.add(rechteck7);
		controller.add(rechteck8);
		controller.add(rechteck9);

		System.out.println(controller.toString());
		System.out.println(controller.toString().equals("BunteRechteckeController [rechtecke=[Rechteck [x=10, y=10, breite=40, hoehe=30], Rechteck [x=25, y=25, breite=20, hoehe=100], Rechteck [x=260, y=10, breite=100, hoehe=200], Rechteck [x=5, y=500, breite=25, hoehe=300], Rechteck [x=100, y=100, breite=100, hoehe=100], Rechteck [x=200, y=200, breite=200, hoehe=200], Rechteck [x=800, y=400, breite=20, hoehe=20], Rechteck [x=800, y=450, breite=20, hoehe=20], Rechteck [x=850, y=400, breite=20, hoehe=20], Rechteck [x=855, y=455, breite=25, hoehe=25]]]"));
	}
}
