package model;

public class Rechteck {
private int x;
private int y;
private int hoehe;							//Attribute
private int breite;

public Rechteck(int x, int y, int hoehe, int breite) {
	this.setX(x);
	this.setY(y);
	this.setHoehe(hoehe);				//Parametriesierter Konstruktor
	this.setBreite(breite);			
}

public Rechteck() {
	this.setX(0);
	this.setY(0);
	this.setHoehe(0);					//Parameterloser Konstruktor
	this.setBreite(0);
}


										//geter/seter
public int getBreite() {
	return breite;
}

public void setBreite(int breite) {
	this.breite = breite;
}

public int getHoehe() {
	return hoehe;
}

public void setHoehe(int hoehe) {
	this.hoehe = hoehe;
}

public int getY() {
	return y;
}

public void setY(int y) {
	this.y = y;
}

public int getX() {
	return x;
}

public void setX(int x) {
	this.x = x;
}
@Override
public String toString() {
	return "Rechteck [x=" + x + ", y=" + y + ", breite=" + breite + ", hoehe=" + hoehe + "]";
}
}
