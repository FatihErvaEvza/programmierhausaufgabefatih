package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
import controller.BunteRechteckeController;

public class Zeichenflaeche extends JPanel {
private final BunteRechteckeController CONTROLLERPANEL;    
public Zeichenflaeche(BunteRechteckeController controller) {
CONTROLLERPANEL = controller;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(Color.MAGENTA);
    	g.setColor(Color.BLACK);
    	for (int i = 0; i < CONTROLLERPANEL.getRechtecke().size(); i++) {
		g.drawRect(CONTROLLERPANEL.getRechtecke().get(i).getX(),CONTROLLERPANEL.getRechtecke().get(i).getY(),CONTROLLERPANEL.getRechtecke().get(i).getBreite(),CONTROLLERPANEL.getRechtecke().get(i).getHoehe());	
		}
    }


}